# Td Ia

## Lancement du programme
La commande de lancement du programme utilise pipenv et est localisée dans .vscode/tasks.json.

## Projets associés
- [Notes de cours](https://gitlab.etude.cy-tech.fr/kleinarthu/semestre4)
- [Tp de programmation système](https://gitlab.etude.cy-tech.fr/kleinarthu/td_progSys)
- [Tp de programmation parallèle](https://gitlab.etude.cy-tech.fr/kleinarthu/td_progPar)
- [Tp de programmation fonctionnelle](https://gitlab.etude.cy-tech.fr/kleinarthu/td_progFonc)
- [Tp d'IA Algorithmes](https://gitlab.etude.cy-tech.fr/kleinarthu/td_ia)
